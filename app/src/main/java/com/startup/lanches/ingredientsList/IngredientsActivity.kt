package com.startup.lanches.ingredientsList

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.startup.lanches.R
import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.entities.view.ViewSandwich
import com.startup.lanches.sandwichDetails.SANDWICH
import com.startup.lanches.sandwichList.IngredientsListAdapter
import kotlinx.android.synthetic.main.activity_ingredient.*

const val INGREDIENTS_ACTIVITY = 1
const val INGREDIENTS = "INGREDIENTS"

class IngredientsActivity : AppCompatActivity(), IngredientContract.View {

    var presenter: IngredientContract.Presenter? = null
    lateinit var adapter: IngredientsListAdapter

    lateinit var sandwich: ViewSandwich;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ingredient)
        presenter = IngredientsPresenter(this)

        sandwich = intent.extras.getSerializable(SANDWICH) as ViewSandwich

        ingredients_rv.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        adapter = IngredientsListAdapter();
        ingredients_rv.adapter = adapter;

        return_bt.setOnClickListener {

            val ingredients: Array<Ingredient> = adapter.list.filter { it.count > 0 }.toTypedArray()
            var intent: Intent = Intent()
            intent.putExtra(INGREDIENTS, ingredients)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.requestIngredientsList()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
        presenter = null
    }

    override fun listIngredientsError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun listIngredients(list: List<Ingredient>) {

        for (sandIng in sandwich.extras) {
            var ing = list.find { it.id == sandIng.id }
            ing?.count = sandIng.count;
        }

        adapter.list = list;
        adapter.notifyDataSetChanged()
    }


}
