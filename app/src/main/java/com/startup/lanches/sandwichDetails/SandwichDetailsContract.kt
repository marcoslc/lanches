package com.startup.lanches.sandwichList

import android.content.Context
import com.startup.lanches.entities.Sandwich
import com.startup.lanches.entities.view.ViewSandwich

/**
 * Created by marcoscardoso on 26/08/17.
 */
interface SandwichLDetailsContract {
    interface View {
        fun sandwichSavedWithSuccess();
        fun errorSavingSandwich();

    }

    interface Presenter {
        fun onDestroy()
        fun goToIngredientsList(context: Context, sandwich: ViewSandwich);
        fun calculatePromotions(sandwich: ViewSandwich)
        fun goToShopCart()
        fun saveSandwichOnServer(sandwich: ViewSandwich)
    }

    interface Interactor {
        fun saveSandwichOnServer(sandwich: ViewSandwich, output: InteractorOutput)
    }

    interface InteractorOutput {
        fun success()
        fun error()
    }
}