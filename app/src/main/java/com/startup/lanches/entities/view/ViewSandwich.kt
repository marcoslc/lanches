package com.startup.lanches.entities.view

import java.io.Serializable

data class ViewSandwich(val id: Int, val name: String, val image: String, val ingredients: List<Ingredient>, var extras: List<Ingredient>) : Serializable {
    val oririginalPrice: Double
        get() = ingredients.sumByDouble { it.price * it.count }

    val fullPrice: Double
        get() = ingredients.sumByDouble { it.price * it.count } + extras.sumByDouble { it.price * it.count }

    var discountPrice: Double = 0.0

}