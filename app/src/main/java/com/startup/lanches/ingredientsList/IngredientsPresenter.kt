package com.startup.lanches.ingredientsList

import com.startup.lanches.entities.view.Ingredient


class IngredientsPresenter(val view: IngredientContract.View) : IngredientContract.Presenter {
    var interactor: IngredientInteractor? = IngredientInteractor()

    override fun onDestroy() {
        interactor = null;
    }

    override fun requestIngredientsList() {

        val output = object : IngredientInteractor.IngredientsRequestOutput {

            override fun success(list: List<Ingredient>) {
                view.listIngredients(list)
            }

            override fun error() {
                view.listIngredientsError()
            }
        }

        interactor?.getIngredients(output)
    }

}