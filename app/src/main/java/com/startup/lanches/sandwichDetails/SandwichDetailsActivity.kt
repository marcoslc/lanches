package com.startup.lanches.sandwichDetails

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.startup.lanches.R
import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.entities.view.ViewSandwich
import com.startup.lanches.ingredientsList.INGREDIENTS
import com.startup.lanches.ingredientsList.INGREDIENTS_ACTIVITY
import com.startup.lanches.sandwichList.SandwichLDetailsContract
import kotlinx.android.synthetic.main.activity_sandwich_details.*

const val SANDWICH: String = "SANDWICH"

class SandwichDetailsActivity : AppCompatActivity(), SandwichLDetailsContract.View {

    var presenter: SandwichDetailsPresenter? = null;
    lateinit var sandwich: ViewSandwich

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sandwich_details)
        presenter = SandwichDetailsPresenter(this);

        sandwich = intent.extras.getSerializable(SANDWICH) as ViewSandwich

        setupView(sandwich)

        customize_bt.setOnClickListener { presenter?.goToIngredientsList(this, sandwich) }
        add_to_cart.setOnClickListener { presenter?.saveSandwichOnServer(sandwich) }
    }

    fun setupView(sandwich: ViewSandwich) {
        sandwich_image.setImageURI(sandwich.image)
        sandwich_title.text = sandwich.name

        sandwich_price.text = "R$: ${"%.2f".format(sandwich.oririginalPrice)}"
        full_value.text = "Valor Total R$ ${"%.2f".format(sandwich.fullPrice)}"

        sandwich_ingredients.text = sandwich.ingredients.joinToString("\n", prefix = "Ingredientes: \n\n") { it.name }
        sandwich_ingredients_extras.text = sandwich.extras.joinToString("\n", prefix = "Ingredientes Extras: \n\n") { "${it.count} - ${it.name}" }


        discount_value.visibility = if (sandwich.discountPrice > 0) View.VISIBLE else View.GONE
        discount_value.text = "Valor com desconto R$ ${"%.2f".format(sandwich.discountPrice)}"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == INGREDIENTS_ACTIVITY && resultCode == Activity.RESULT_OK) {
            var ingredients: Array<Ingredient> = data?.getSerializableExtra(INGREDIENTS) as Array<Ingredient>
            sandwich.extras = ingredients.toList()
            presenter?.calculatePromotions(sandwich)
            setupView(sandwich)
        }
    }

    override fun sandwichSavedWithSuccess() {
    }

    override fun errorSavingSandwich() {
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }
}
