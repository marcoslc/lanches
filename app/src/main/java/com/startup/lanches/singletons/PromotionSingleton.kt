package com.startup.lanches.singletons

import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.entities.Promotion
import com.startup.lanches.entities.view.ViewSandwich

/**
 * Created by marcoscardoso on 27/08/17.
 */
object PromotionSingleton {
    var list: ArrayList<Promotion> = ArrayList<Promotion>()


    fun applyPromotion(id: Int, sandwich: ViewSandwich): Pair<Double, Double> {

        val unifiedList: List<Ingredient> = unifyingList(sandwich)

        return when (id) {
            1 -> getDiscount1IfApplicable(unifiedList)
            2 -> getDiscount2IfApplicable(unifiedList)
            3 -> getDiscount3IfApplicable(unifiedList)
            else -> Pair(0.0, 0.0)
        }
    }

    fun getDiscount1IfApplicable(list: List<Ingredient>): Pair<Double, Double> {
        if (list.find { it.id == 1 } != null && list.find { it.id == 2 } == null) {
            return Pair(0.0, 0.1)
        }
        return Pair(0.0, 0.0)
    }

    fun getDiscount2IfApplicable(list: List<Ingredient>): Pair<Double, Double> {
        var meat: Ingredient? = list.find { it.id == 3 }

        if (meat == null) return Pair(0.0, 0.0)

        var num = meat.count / 3
        var discount: Double = num * meat.price;

        return Pair(discount, 0.0)
    }

    fun getDiscount3IfApplicable(list: List<Ingredient>): Pair<Double, Double> {
        var cheese: Ingredient? = list.find { it.id == 5 }

        if (cheese == null) return Pair(0.0, 0.0)

        var num = cheese.count / 3
        var discount: Double = num * cheese.price;

        return Pair(discount, 0.0)
    }

    fun applyAllPromotions(sandwich: ViewSandwich) {
        var pair: Pair<Double, Double> = Pair(0.0, 0.0)
        for (promotion in list) {
            var discount = applyPromotion(promotion.id, sandwich)
            pair = Pair(pair.first + discount.first, pair.second + discount.second)
        }

        sandwich.discountPrice = (sandwich.fullPrice - pair.first) * (1.0 - pair.second)
    }

    fun unifyingList(sandwich: ViewSandwich): List<Ingredient> {
        var unifiedList = sandwich.ingredients.map { it.copy() }

        for (extraIng in sandwich.extras) {
            var ing = unifiedList.find { it.id == extraIng.id }

            if (ing != null)
                ing.count += extraIng.count;
            else
                unifiedList += extraIng
        }

        return unifiedList;
    }
}