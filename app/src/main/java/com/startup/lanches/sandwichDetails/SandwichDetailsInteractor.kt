package com.startup.lanches.sandwichDetails

import com.startup.lanches.api.APIFactory
import com.startup.lanches.entities.Sandwich
import com.startup.lanches.entities.view.ViewSandwich
import com.startup.lanches.sandwichList.SandwichLDetailsContract
import com.startup.lanches.singletons.SandwichSingleton
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

/**
 * Created by marcoscardoso on 27/08/17.
 */

class SandwichDetailsInteractor : SandwichLDetailsContract.Interactor {
    override fun saveSandwichOnServer(sandwich: ViewSandwich, output: SandwichLDetailsContract.InteractorOutput) {
    }
}
