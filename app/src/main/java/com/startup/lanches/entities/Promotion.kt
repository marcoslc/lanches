package com.startup.lanches.entities


data class Promotion(val id: Int, val name: String, val description: String)