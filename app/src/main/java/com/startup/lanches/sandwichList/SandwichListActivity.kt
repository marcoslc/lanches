package com.startup.lanches.sandwichList

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import com.startup.lanches.R
import com.startup.lanches.entities.Sandwich
import com.startup.lanches.entities.view.ViewSandwich
import com.startup.lanches.sandwichDetails.SandwichDetailsActivity
import kotlinx.android.synthetic.main.activity_main.*

class SandwichListActivity : SandwichListContract.View, AppCompatActivity() {
    var presenter: SandwichListPresenter? = null

    lateinit var adapter: SandWichListAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = SandwichListPresenter(this);

        sandwich_rc.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        adapter = SandWichListAdapter();
        sandwich_rc.adapter = adapter;

        var onclick = object : OnRecyclerCliked {
            override fun clicked(sandwich: ViewSandwich) {
                presenter?.goToSandwichDetail(this@SandwichListActivity, sandwich)
            }
        }

        adapter.onClick = onclick
    }

    override fun onResume() {
        super.onResume()
        presenter?.requestSandwichList();
    }

    override fun errorOnRequestSandwich() {
        Log.v("Sandwich List error", "error")
    }

    override fun sandwichListReceived(list: List<ViewSandwich>) {
        adapter.list = list;
        adapter.notifyDataSetChanged()
    }

    override fun onSandwichClicked() {

    }

    override fun onDestroy() {
        presenter?.onDestroy()
        presenter = null
        super.onDestroy()
    }

    interface OnRecyclerCliked {
        fun clicked(sandwich: ViewSandwich)
    }
}
