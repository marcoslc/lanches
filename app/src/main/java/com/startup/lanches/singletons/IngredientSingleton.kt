package com.startup.lanches.singletons

import com.startup.lanches.entities.view.Ingredient

/**
 * Created by marcoscardoso on 26/08/17.
 */

object IngredientSingleton {
    var list: ArrayList<Ingredient> = ArrayList<Ingredient>()

    fun getListBasedById(ingredients: IntArray): List<Ingredient?> {
        var newList: List<Ingredient?> = ArrayList<Ingredient>()

        for (ingredient in ingredients) {
            var ing = list.find { it.id == ingredient }
            ing?.count = 1;
            newList += ing
        }

        return newList.filterNotNull();
    }
}