package com.startup.lanches;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by marcoscardoso on 26/08/17.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }

}
