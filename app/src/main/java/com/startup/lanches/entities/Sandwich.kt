package com.startup.lanches.entities

/**
 * Created by marcoscardoso on 26/08/17.
 */

data class Sandwich(val id: Int, val name: String, val image: String, val ingredients: IntArray)