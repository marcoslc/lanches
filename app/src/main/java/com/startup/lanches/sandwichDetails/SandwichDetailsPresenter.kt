package com.startup.lanches.sandwichDetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.startup.lanches.entities.view.ViewSandwich
import com.startup.lanches.ingredientsList.INGREDIENTS_ACTIVITY
import com.startup.lanches.ingredientsList.IngredientsActivity
import com.startup.lanches.sandwichList.SandwichLDetailsContract
import com.startup.lanches.singletons.PromotionSingleton

/**
 * Created by marcoscardoso on 26/08/17.
 */

class SandwichDetailsPresenter(var view: SandwichLDetailsContract.View) : SandwichLDetailsContract.Presenter {
    var interactor: SandwichLDetailsContract.Interactor = SandwichDetailsInteractor()

    override fun goToIngredientsList(context: Context, sandwich: ViewSandwich) {
        val intent: Intent = Intent(context, IngredientsActivity::class.java)
        intent.putExtra(SANDWICH, sandwich)

        if (context is Activity) {
            context.startActivityForResult(intent, INGREDIENTS_ACTIVITY)
        }
    }

    override fun calculatePromotions(sandwich: ViewSandwich) {
        PromotionSingleton.applyAllPromotions(sandwich);
    }

    override fun saveSandwichOnServer(sandwich: ViewSandwich) {

        var output: SandwichLDetailsContract.InteractorOutput = object : SandwichLDetailsContract.InteractorOutput {
            override fun success() {
                view.sandwichSavedWithSuccess()
            }

            override fun error() {
                view.errorSavingSandwich()
            }
        }

        interactor.saveSandwichOnServer(sandwich, output)
    }

    override fun goToShopCart() {}

    override fun onDestroy() {}
}
