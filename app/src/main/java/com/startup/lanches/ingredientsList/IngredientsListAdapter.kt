package com.startup.lanches.sandwichList

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.startup.lanches.R
import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.inflate
import kotlinx.android.synthetic.main.list_item_ingredients.view.*

class IngredientsItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun setupView(adapter: RecyclerView.Adapter<IngredientsItemHolder>, ingredient: Ingredient) = with(itemView) {
        ingredient_title.text = ingredient.name
        ingredient_price.text = "R$ ${"%.2f".format(ingredient.price)}"
        ingredient_count.text = "${ingredient.count}"
        minus_bt.setOnClickListener { if (ingredient.count > 0) ingredient.count-- else ingredient.count = 0; adapter.notifyDataSetChanged() }
        plus_bt.setOnClickListener { ingredient.count++; adapter.notifyDataSetChanged() }
    }
}

class IngredientsListAdapter : RecyclerView.Adapter<IngredientsItemHolder>() {
    var list: List<Ingredient> = ArrayList<Ingredient>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = IngredientsItemHolder(parent.inflate(R.layout.list_item_ingredients))

    override fun onBindViewHolder(holder: IngredientsItemHolder, position: Int) = holder.setupView(this, list[position])

    override fun getItemCount() = list.size
}