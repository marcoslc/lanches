# README #

## O que é esse repositóro? ##

* Este app é um sistema simples de venda de sandwiches
* Este Projeto foi construido baseado em uma Clean Architecture, uma versão simplificada do VIPER. 
* A linguagem base é o kotlin.

## O que eu preciso para rodar o projeto? (Recommended) ##

* Android Studio 2.2+
* Support for kotlin projects
* Android API 26
* node server

## Como executar esse projeto? ##

### Server ###

* go to folder {projectbasefolder}/server
* execute "npm install"
* execute "node main.js"

### App ###

* import on AS
* execute