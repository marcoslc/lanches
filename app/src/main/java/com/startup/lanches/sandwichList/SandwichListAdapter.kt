package com.startup.lanches.sandwichList

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.startup.lanches.R
import com.startup.lanches.entities.view.ViewSandwich
import com.startup.lanches.inflate
import com.startup.lanches.sandwichList.SandwichListActivity.OnRecyclerCliked
import kotlinx.android.synthetic.main.list_item_sandwitch.view.*

/**
 * Created by marcoscardoso on 26/08/17.
 */
class SandwichListItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun setupView(onClick: OnRecyclerCliked?, sandwich: ViewSandwich) = with(itemView) {
        sandwich_image.setImageURI(sandwich.image)
        sandwich_title.text = sandwich.name
        sandwich_price.text = "R$ ${"%.2f".format(sandwich.fullPrice)}"
        sandwich_ingredients.text = sandwich.ingredients.joinToString(prefix = "Ingredientes: ") { it.name }

        sandwich_root.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                onClick?.clicked(sandwich)
            }
        })

    }
}

class SandWichListAdapter : RecyclerView.Adapter<SandwichListItemHolder>() {
    var list: List<ViewSandwich> = ArrayList<ViewSandwich>() as List<ViewSandwich>

    var onClick: OnRecyclerCliked? = null;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SandwichListItemHolder(parent.inflate(R.layout.list_item_sandwitch))

    override fun onBindViewHolder(holder: SandwichListItemHolder, position: Int) = holder.setupView(this.onClick, list[position])

    override fun getItemCount() = list.size

}