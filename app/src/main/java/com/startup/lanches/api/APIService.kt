package com.startup.lanches.api

import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.entities.Promotion
import com.startup.lanches.entities.Sandwich

import retrofit2.Call
import retrofit2.http.*

interface APIService {

    @GET("api/lanche")
    fun listSandwich(): Call<List<Sandwich>>

    @GET("api/ingrediente")
    fun listIngredients(): Call<List<Ingredient>>

    @GET("api/promocao")
    fun listPromotions(): Call<List<Promotion>>
}
