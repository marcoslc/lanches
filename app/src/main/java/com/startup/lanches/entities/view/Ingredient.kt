package com.startup.lanches.entities.view

import java.io.Serializable

/**
 * Created by marcoscardoso on 26/08/17.
 */

data class Ingredient(val id: Int, val name: String, val price: Double, val image: String, var count: Int) : Serializable
