package com.startup.lanches.ingredientsList

import com.startup.lanches.api.APIFactory
import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.singletons.IngredientSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by marcoscardoso on 26/08/17.
 */

class IngredientInteractor : IngredientContract.Interactor {
    override fun getIngredients(output: IngredientsRequestOutput) {
        output.success(IngredientSingleton.list)

        val callback = object : Callback<List<Ingredient>> {
            override fun onResponse(call: Call<List<Ingredient>>?, response: Response<List<Ingredient>>?) {
                if (response?.body() != null) {
                    IngredientSingleton.list = response.body() as ArrayList<Ingredient>
                    output.success(response.body() as List<Ingredient>)
                } else {
                    output.error()
                }
            }

            override fun onFailure(call: Call<List<Ingredient>>?, t: Throwable?) {
                output.error()
            }
        }
        APIFactory.service.listIngredients().enqueue(callback)
    }

    interface IngredientsRequestOutput {
        fun success(ingredients: List<Ingredient>)
        fun error();
    }
}
