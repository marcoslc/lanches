package com.startup.lanches.sandwichList

import com.startup.lanches.api.APIFactory
import com.startup.lanches.entities.Sandwich
import com.startup.lanches.singletons.SandwichSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SandwichListInteractor(var presenter: SandwichListPresenter?) : SandwichListContract.Interactor {

    override fun getSandwichList(output: MyHamburguersListOutput) {

        output.success(SandwichSingleton.list)

        val callback = object : Callback<List<Sandwich>> {
            override fun onResponse(call: Call<List<Sandwich>>?, response: Response<List<Sandwich>>?) {
                if (response?.body() != null) {
                    SandwichSingleton.list = response.body() as ArrayList<Sandwich>
                    output.success(response.body() as List<Sandwich>)
                } else {
                    output.error()
                }
            }

            override fun onFailure(call: Call<List<Sandwich>>?, t: Throwable?) {
                output.error()
            }
        }

        APIFactory.service.listSandwich().enqueue(callback)
    }


    interface MyHamburguersListOutput {
        fun success(list: List<Sandwich>);
        fun error();
    }
}
