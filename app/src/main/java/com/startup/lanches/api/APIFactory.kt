package com.startup.lanches.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by marcoscardoso on 26/08/17.
 */

object APIFactory {

    val service: APIService
        get() {
            val retrofit = Retrofit.Builder()
                    .baseUrl("http://10.221.122.121:8080")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(APIService::class.java)
        }
}
