package com.startup.lanches.sandwichList

import android.content.Context
import android.content.Intent
import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.entities.Sandwich
import com.startup.lanches.entities.view.ViewSandwich
import com.startup.lanches.sandwichDetails.SANDWICH
import com.startup.lanches.sandwichDetails.SandwichDetailsActivity
import com.startup.lanches.singletons.IngredientSingleton
import java.util.ArrayList

class SandwichListPresenter(var view: SandwichListContract.View?) : SandwichListContract.Presenter {
    var interactor: SandwichListContract.Interactor? = SandwichListInteractor(this)

    override fun requestSandwichList() {

        val output = object : SandwichListInteractor.MyHamburguersListOutput {

            override fun success(list: List<Sandwich>) {
                val newList: List<ViewSandwich> = list.map {
                    ViewSandwich(it.id, it.name, it.image, IngredientSingleton.getListBasedById(it.ingredients) as List<Ingredient>, ArrayList<Ingredient>())
                }
                view?.sandwichListReceived(newList);
            }

            override fun error() {
                view?.errorOnRequestSandwich()
            }
        }

        interactor?.getSandwichList(output)
    }

    override fun goToSandwichDetail(context: Context, sandwich: ViewSandwich) {
        val intent: Intent = Intent(context, SandwichDetailsActivity::class.java)
        intent.putExtra(SANDWICH, sandwich)
        context.startActivity(intent)
    }

    fun onDestroy() {
        view = null
        interactor = null
    }
}