package com.startup.lanches.sandwichList

import android.content.Context
import com.startup.lanches.entities.Sandwich
import com.startup.lanches.entities.view.ViewSandwich

/**
 * Created by marcoscardoso on 26/08/17.
 */
interface SandwichListContract {
    interface View {
        fun sandwichListReceived(list: List<ViewSandwich>)
        fun onSandwichClicked()
        fun errorOnRequestSandwich()
    }

    interface Presenter {
        fun requestSandwichList()
        fun goToSandwichDetail(context: Context, sandwich: ViewSandwich)
    }

    interface Interactor {
        fun getSandwichList(output: SandwichListInteractor.MyHamburguersListOutput);
    }
}