package com.startup.lanches.ingredientsList

import com.startup.lanches.entities.view.Ingredient

/**
 * Created by marcoscardoso on 26/08/17.
 */

class IngredientContract {


    interface View {
        fun listIngredients(list: List<Ingredient>)
        fun listIngredientsError()
    }

    interface Presenter {
        fun onDestroy()
        fun requestIngredientsList()
    }

    interface Interactor {
        fun getIngredients(output: IngredientInteractor.IngredientsRequestOutput);
    }
}
