package com.startup.lanches

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.startup.lanches.api.APIFactory
import com.startup.lanches.entities.view.Ingredient
import com.startup.lanches.entities.Promotion
import com.startup.lanches.entities.Sandwich
import com.startup.lanches.sandwichList.SandwichListActivity
import com.startup.lanches.singletons.IngredientSingleton
import com.startup.lanches.singletons.PromotionSingleton
import com.startup.lanches.singletons.SandwichSingleton

import retrofit2.Call
import retrofit2.Response

class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        requestIngredientsAndSandwich()
    }

    fun requestIngredientsAndSandwich() {
        APIFactory.service.listIngredients().enqueue(object : retrofit2.Callback<List<Ingredient>> {
            override fun onResponse(call: Call<List<Ingredient>>?, response: Response<List<Ingredient>>) {
                if (response.body() != null) {
                    IngredientSingleton.list = response.body() as ArrayList<Ingredient>
                    requestSandwichFromAPIAndGoToListSandwich()
                } else {
                }
            }

            override fun onFailure(call: Call<List<Ingredient>>?, t: Throwable?) {
            }
        })
    }

    private fun requestSandwichFromAPIAndGoToListSandwich() {
        APIFactory.service.listSandwich().enqueue(object : retrofit2.Callback<List<Sandwich>> {
            override fun onResponse(call: Call<List<Sandwich>>?, response: Response<List<Sandwich>>?) {
                if (response?.body() != null) {
                    SandwichSingleton.list = response?.body() as List<Sandwich>;
                    requestPromotions()
                } else {
                }
            }

            override fun onFailure(call: Call<List<Sandwich>>?, t: Throwable?) {
            }
        })
    }

    fun requestPromotions() {
        APIFactory.service.listPromotions().enqueue(object : retrofit2.Callback<List<Promotion>> {
            override fun onResponse(call: Call<List<Promotion>>?, response: Response<List<Promotion>>?) {
                if (response?.body() != null) {
                    PromotionSingleton.list = response?.body() as ArrayList<Promotion>;
                    goToListSandwich()
                } else {
                }
            }

            override fun onFailure(call: Call<List<Promotion>>?, t: Throwable?) {
            }
        })
    }

    private fun goToListSandwich() {
        var intent: Intent = Intent(this, SandwichListActivity::class.java)
        startActivity(intent)
        finish()
    }
}
